import * as Location from "expo-location";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import Weather from "./components/weather";
import beachbackgroundtest from './assets/beachbackgroundtest.jpeg';
import GooglePlacesInput from './components/location'
// import axios from 'axios'
// import CurrentLocation from './components/location'
// not called or used here yet

import { weather_API_KEY } from "./APIkeys";

const App = () => {
  const [temperature, setTemperature] = useState(0);
  const [location, setLocation] = useState(null);
  const [lat, setLat] = useState(null);
  const [lng, setLng] = useState(null)



// this commented out to prevent errors?
  // const fetchWeather = async ({lat , lon  }) => {
  

  //   fetch(
  //     `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${weather_API_KEY}&units=metric`
  //   )
  //     .then((res) => res.json())
  //     .then((json) => {
  //       console.log(json.main);
  //       let tempTemp = json.main.temp;
  //       Math.round(tempTemp)
  //       setTemperature(tempTemp);
  //       console.log(temperature)
  // }

  //     )};

  useEffect(() => {
    const setCurrentLocation = async () => {
      try {
        let { status } = await Location.requestForegroundPermissionsAsync();
        // if (status !== "granted") {
        //   setErrorMsg("Permission to access location was denied");
        //   return;
        // }
        let location = await Location.getCurrentPositionAsync({});
        console.log("location: ", location);
        setLocation(location);
        setLng(location.coords.longitude)
        setLat(location.coords.latitude)
        console.log(lat)
        console.log(lng)
      } 
      catch (err) {
        console.info(err);
      }
    };
    setCurrentLocation();
  }, []);

  return (
   
    <View style={styles.container}>
      <Text style={styles.title}>Ye Olde Weather Adventure Guide</Text>
      <Weather location={location} lat={lat} lng={lng}/>
    </View>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: '10%'
    // backgroundImage: "orange",
  },
  // backgroundImage: {
  //   flex: 1,
  //   justifyContent: "center",
  //   width: '100%',
  //   height: '100%'
  
  // },
  title: {
    textAlign: 'center',
    fontSize: 40,
    fontFamily:'Avenir'
  },

  standardText: {
    color: "black",
    alignItems: "center",
    justifyContent: "center",
  }
});

export default App;




//GRAVEYARD OF TRIED THINGS: 

  // componentDidMount() {
  //   navigator.geolocation.getCurrentPosition(
  //     position => {
  //       this.fetchWeather(position.coords.latitude, position.coords.longitude);
  //     },
  //     error => {
  //       this.setState({
  //         error: 'Error Getting Weather Condtions'
  //       });
  //     }
  //   );
  // }

  // findCoordinates = () => {
	// 	navigator.geolocation.getCurrentPosition(
	// 		position => {
	// 			const location = JSON.stringify(position);

	// 			this.setState({ location });
	// 		},
	// 		error => Alert.alert(error.message),
	// 	);
	// };

  

  // fetchWeather(lat, lon) {
  //   fetch(
  //     `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${weather_API_KEY}&units=metric`
  //   )
  //     .then(res => res.json())
  //     .then(json => {
  //       // console.log(json);
  //       this.setState({
  //         temperature: json.main.temp,
  //         weatherCondition: json.weather[0].main,
  //         isLoading: false
  //       });
  //     });
  // }

  // // import * as Location from "expo-location";

  // 
// var options = {
//   enableHighAccuracy: true,
//   timeout: 5000,
//   maximumAge: 0
// };

// function success(pos) {
//   var crd = pos.coords;

//   console.log('Your current position is:');
//   console.log(`Latitude : ${crd.latitude}`);
//   console.log(`Longitude: ${crd.longitude}`);
//   console.log(`More or less ${crd.accuracy} meters.`);
// }

// function error(err) {
//   console.warn(`ERROR(${err.code}): ${err.message}`);
// }

// navigator.geolocation.getCurrentPosition(success, error, options);
//  {isLoading ? (
//   <View style={styles.loadingContainer}>
//   <CurrentLocation/>
// </View>
// ) : (
// <Weather weather={weatherCondition} temperature={temperature} />

// )}