


import React, { useState, useEffect, useRef } from "react";
import { StyleSheet, Text, View, Image, Button } from "react-native";
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import gPlaces_KEY from  './../APIkeys';

const APIKEY = '35f7e8802740bc0849d1b36e13127af5'


const GooglePlacesInput = (props) => {

  
 const [gLong, setgLong] = useState(null)
 const [gLat, setgLat] =  useState(null)
 const [tempTemp1, settempTemp1] = useState(null)

  

const ref=useRef();


useEffect(()=>{
  console.log('lat,long',gLat,gLong)
  if (gLat == null || gLong == null) {
return;
  }
  fetchWeather()
},[[gLat,gLong]])



const fetchWeather = async (  ) => {

  
console.log(gLat, gLong)
  fetch(
    `http://api.openweathermap.org/data/2.5/weather?lat=${gLat}&lon=${gLong}&APPID=${APIKEY}&units=metric`
  )
    .then((res) => res.json())
    .then((json) => {
      console.log(json)
      let tempTemp = Math.round(json.main.temp); 
      props.changeWeather(tempTemp);
      let tempLow = Math.round(json.main.temp_min);
      props.changeLow(tempLow)
      let tempHigh = Math.round(json.main.temp_max);
      props.changeHigh(tempHigh)
      props.changeMain(json.weather[0].main)


      // let tempLow = Math.round(json.main.temp_min);
      // setWeatherLow(tempLow);
      // let tempHigh = Math.round(json.main.temp_max);
      // setWeatherHigh(tempHigh)
      // console.log(json.weather[0].main)
      // setWeatherMain(json.weather[0].main)
}

    )
  };

// useEffect(() => {
//   ref.current?.setAddressText('sometext');
// },[]
// )




return (
  <View>
  <GooglePlacesAutocomplete
  keyboardShouldPersistTaps={"always"}  
  styles={{
    container: {backgroundColor: 'transparent'},
    // textInputContainer: styles.viewStyle,
    // textInput: styles.textInputStyle,
}}
  placeholder='Search here to utilise GLOBAL MAGIC'
  minLength={2}
  autoFocus={false}
  returnKeyType = {'default'}
  listViewDisplayed='auto'
  fetchDetails={true}
  onPress={(data, details = null) => {

    console.log(details.geometry)
    setgLat(details.geometry.location.lat)
    setgLong(details.geometry.location.lng)
    console.log(gLat, gLong)
    // fetchWeather ()
    
  }}
 
      getDefaultValue = {() => {
        return ''
      }}
      query = {{
        key: 'AIzaSyDftU4HpIeEr1r-saL0DZtJ0MPx5BU3OPo',
        language: 'en',
        types: '(cities)',
      }}
      requestUrl={{
        useOnPlatform: 'web', // or "all"
        url:
          'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api', // or any proxy server that hits https://maps.googleapis.com/maps/api
        headers: {
          Authorization: `an auth token`, // if required for your proxy
        },}}
      style = {{
      }}
      currentLocation = {false}
      currentLocationLabel = 'Current location'
      debounce = {300}
      />
    </View>
  );

};


const styles = StyleSheet.create({
    padding: {
      padding: 50,
      marginBottom: 50
    }

})


export default GooglePlacesInput


