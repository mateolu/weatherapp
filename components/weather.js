import React, { useRef, useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  TouchableOpacity,
  Button,
  ImageBackground,
}  from 'react-native'
import { weather_API_KEY } from "./../APIkeys";
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import GooglePlacesInput from "./location";



import {MaterialCommunityIcons} from '@expo/vector-icons';
import PropTypes from 'prop-types';
import {weatherConditions} from './../weatherConditions'

const Weather = (props) => {


  
    const [temperature, setTemperature] = useState(null);
    const [weatherMain, setWeatherMain] = useState('Clear');
    const [weatherLow, setWeatherLow] = useState(null);
    const [weatherHigh, setWeatherHigh] = useState(null);
    const [subtitle, setSubtitle] = useState(null)
    const [nlat, setnLat] = useState(null);
    const [nlong, setnLong] = useState(null)

    let changeWeather = (e) => {
        setTemperature(e)
    }

    let changeMain = (e) => {
        setWeatherMain(e)
    }

    let changeLow = (e) => {
        setWeatherLow(e)
    }
    
    let changeHigh = (e) => {
        setWeatherHigh(e)
    }



    const weatherCases = {
        'Clear': {
            title: 'clear', 
            background: require('./../assets/beachback.jpeg')

        },
        'Rain': {
            title: 'rain',
            background: require ('./../assets/rainy-1 copia.png')
        },
        'Clouds': {
            title: 'clouds',
            background: require ('./../assets/rainy-1.png')
        },
        'Drizzle': {
            title: 'drizzle',
            background: require ('./../assets/drizzle.png')
        },
        'Thunderstorm': {
            title: 'thunderstorm',
            background: require ('./../assets/night-clear-1.png')
        }
        
    }


    useEffect(() => {
        
        const testFunction = (props) => {
            if (location != null) {
             setnLat (41)
            //  This and below  will be taken from app.js, current location button
             setnLong(2)
             console.log (nlat)
             console.log (nlong)
            }
            else if (location != null && props.lat == null) {
                setnLat(gLat)
                setnLong(gLong)
                console.log (nlat)
                console.log (nlong)
            }
        }
        
        const test123 = () => {
            if (weatherMain == undefined) {
                setWeatherMain('Clear')
            }
        }
       


        const renderSubtitle = async () => {
            try {
                if (weatherMain == 'Clear') {
                    setSubtitle('The skye is clear! Go forth!')
                    return;
                }
                else if (weatherMain == 'Rain') {
                    setSubtitle('Taketh care that thine bowstring stayeth dry')
                }
                else if (weatherMain == 'Thunderstorm') {
                    setSubtitle('Findeth shelter!')
                } 
                else if (weatherMain == 'Drizzle') {
                    setSubtitle("Stayeth thy course. T'is but a drizzle!")
                }
                else if(weatherMain =='Clouds') {
                    setSubtitle('Useth a compass and navigate thyself with care.')
                }
            }
            catch (err) {
                console.info(err)
            }
            
        };
        test123();
        renderSubtitle();
        testFunction();
})

useEffect(() => {
    console.log("props: ", props);
  }, [props]);
    
 

    const fetchWeather = async ( ) => {


        // const res = await axios.get(
        //   `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${weather_API_KEY}&units=metric`
        // );
    
        // console.log(res);
    
        fetch(
          `http://api.openweathermap.org/data/2.5/weather?lat=${nlat}&lon=${nlong}&APPID=${weather_API_KEY}&units=metric`
        )
          .then((res) => res.json())
          .then((json) => {
            let tempTemp = Math.round(json.main.temp); 
            setTemperature(tempTemp);
            let tempLow = Math.round(json.main.temp_min);
            setWeatherLow(tempLow);
            let tempHigh = Math.round(json.main.temp_max);
            setWeatherHigh(tempHigh)
            console.log(json.weather[0].main)
            setWeatherMain(json.weather[0].main)
      }
    
)};


console.log(weatherCases[weatherMain].background)

return (
    <View style = {styles.body}>
              <ImageBackground style={styles.backgroundImage} source={weatherCases[weatherMain].background}>
      <Text style={styles.standardText}>Through the power of MAGIC thou may clicketh me to know thy current weather condtions</Text>     
       <Button onPress={fetchWeather}  title='Magic'>MAGIC</Button>

       {/* <Text style={styles.standardText}>If thou hast finished thine business here, find thine next adventure's conditions</Text> */}

       <GooglePlacesInput 
       
       changeWeather={changeWeather}
       changeMain = {changeMain}
       changeHigh = {changeHigh}
       changeLow = {changeLow}>
       </GooglePlacesInput>
       
       {/* <Button onPress={fetchWeather} title='Alt Magic'>Alternative Magic</Button> */}

                <Text style={styles.temperature}>The temperature is {temperature} *C & the current conditions are {weatherMain}</Text>
                <Text style={styles.subtitle}>
                        There will be a high of {weatherHigh} * and a low of {weatherLow} *C
                </Text>
                <Text style={styles.subtitle}>{subtitle}</Text>
                
            </ImageBackground>
              
    </View>
        

)
}


  

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    standardText: {
        alignItems: 'center',
        textAlign: 'center',
    },
    body: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    temperature: {
        fontSize: 25,
        color: 'white',
        width: '100%',
        justifyContent: 'center',
        // alignItems: 'center',
        textAlign: 'center',
    },
    title: {
        fontSize: 40,
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    subtitle: {
        fontSize: 20,
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    backgroundImage: {
        flex: 1,
        justifyContent: "center",
        width: '100%',
        height: '100%'
      },
      padding: {
        padding: 50,
        marginBottom: 50}

})

  

export default Weather;



// Graveyard of more code: 
{/* <View style = {styles.container}>
<View style={styles.header}>
    <MaterialCommunityIcons size={50}   name={weatherConditions[weather].icon} color='white'/>
    <Text style={styles.temperature}>{temperature} *</Text>
</View>
<View style={styles.body}> 
    <Text style={styles.title}>{weatherConditions[weather].title}</Text>
    <Text style={styles.subtitle}>
     {weatherConditions[weather].subtitle}
    </Text>
</View>


</View>




)
}

Weather.propTypes = {
temperature: PropTypes.number.isRequired,
weather: PropTypes.string
}; */}


    
// useEffect(() => {
//     const setCurrentLocation = async () => {
//       try {
//         let { status } = await Location.requestForegroundPermissionsAsync();
//         // if (status !== "granted") {
//         //   setErrorMsg("Permission to access location was denied");
//         //   return;
//         // }
//         let location = await Location.getCurrentPositionAsync({});
//         console.log("location: ", location);
//         setLocation(location);
//         console.log(location)
//       } 
//       catch (err) {
//         console.info(err);
//       }
//     };
//     setCurrentLocation();
//   }, []);
